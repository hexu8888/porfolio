import React, { Component } from 'react';
var picture = require('./img/profile.png');

class Header extends Component {
    constructor(props){
        super(props);
        this.state = {
            profilePicture : picture,
            personalIntro : "Interested @ Web Services - Architecture - Automation",
            title : "Software Engineer"
        }
    }
    
    render() {
        return (
            <header>
                <div className="container" id="maincontent" tabIndex="-1">
                    <div className="row">
                        <div className="col-lg-12">
                            <img className="img-responsive" src={this.state.profilePicture} alt="" />
                            <div className="intro-text">
                                <h1 className="name">{this.state.title}</h1>
                                <hr className="star-light"></hr>
                                <span className="skills">{this.state.personalIntro}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            );
    }
}

export default Header;
