
import React, { Component } from 'react';

class Footer extends Component {
  render() {
    return (
        <div>
<footer className="text-center">
        <div className="footer-above">
            <div className="container">
                <div className="row">
                    <div className="footer-col col-md-4">
                        <h3>Location</h3>
                        <p>San Diego</p><p>Irvine</p><p>Los Angeles</p>
                    </div>
                    <div className="footer-col col-md-4">
                        <h3>Follow My Social Media</h3>
                        <ul className="list-inline">
                            <li>
                                <a href="https://www.linkedin.com/in/ken-he/" className="btn-social btn-outline"><span className="sr-only">Linked In</span><i className="fa fa-fw fa-linkedin"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div className="footer-col col-md-4">
                        <h3>About This Porfolio</h3>
                        <p>The purpose of this website is to keep track of my career.</p>
                    </div>
                </div>
            </div>
        </div>
        <div className="footer-below">
            <div className="container">
                <div className="row">
                    <div className="col-lg-12">
                        Copyright &copy; Ken He 2017
                    </div>
                </div>
            </div>
        </div>
    </footer>
        <div class="scroll-top page-scroll hidden-sm hidden-xs hidden-lg hidden-md">
        <a class="btn btn-primary" href="#page-top">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>
        </div>
    );
  }
}

export default Footer;
