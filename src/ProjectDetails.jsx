
            
            import React, { Component } from 'react';
var cabin = require('./img/portfolio/bfh.png');
var cake1 = require('./img/portfolio/cr1.png');
var cake2 = require('./img/portfolio/cr2.png');
var cake3 = require('./img/portfolio/cr3.png');
var circus = require('./img/portfolio/circus.png');
var game = require('./img/portfolio/game.png');
var safe = require('./img/portfolio/safe.png');
var submarine = require('./img/portfolio/submarine.png');

class ProjectDetails extends Component {
  render() {
    return (
        <div>
 <div className="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
        <div className="modal-content">
            <div className="close-modal" data-dismiss="modal">
                <div className="lr">
                    <div className="rl">
                    </div>
                </div>
            </div>
            <div className="container">
                <div className="row">
                    <div className="col-lg-8 col-lg-offset-2">
                        <div className="modal-body">
                            <h2><a href="http://bfhpaint.com">BFHPaint.com</a></h2>
                            <hr className="star-primary"/>
                            <img src={cabin} className="img-responsive img-centered" alt=""/>
                            <p>This is a static website built in React JS. The purpose of this website is to introduce my mom's online paint store. This site is still under enhancement. It will become a dynamic website which can pull the data from database through RESTful web APIs and also update the content through POST and PUT.</p>
                            <ul className="list-inline item-details">
                                <li>Tools: 
                                    <strong>React Js, Webpack, Babel JS, Npm, Express JS, Aliyun, IIS
                                    </strong>
                                </li>
                            </ul>
                            <ul className="list-inline item-details">
           
                                <li>Date: 
                                    <strong><a href="http://startbootstrap.com">August 2017</a>
                                    </strong>
                                </li>
                            </ul>
                            <button type="button" className="btn btn-default" data-dismiss="modal"><i className="fa fa-times"></i> Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div className="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">
        <div className="modal-content">
            <div className="close-modal" data-dismiss="modal">
                <div className="lr">
                    <div className="rl">
                    </div>
                </div>
            </div>
            <div className="container">
                <div className="row">
                    <div className="col-lg-8 col-lg-offset-2">
                        <div className="modal-body">
                            <h2>Code Review Tool</h2>
                            <hr className="star-primary"/>
                            <p>This tool was built in C#. It helps developers making code review comments easier and faster; it boosts developers' speed by 25%. </p><br/>
                            <p>1)First, highlight a piece of code and then right click on it. Next, on the menu, click on "Add Code Review Comment. A message box will pop up for a developer to put comments.</p>
                            <img src={cake1} className="img-responsive img-centered" alt=""/>
                             <p>2)Second, when a developer finishes making comments, just click on "Save Comment". A "Comment Queue" window will show up on the right. It keeps all the details in it.</p>
                            <img src={cake2} className="img-responsive img-centered" alt=""/>
                                                        <p>3)Last, when a developer finishes making code review, he/she can simply clicks on "Send Comment Email" button. Then it will launch Outlook with all and nice formatted comments. He/She will need to put the receivers' emails and send it.</p>
                            <img src={cake3} className="img-responsive img-centered" alt=""/>
                                                                           <ul className="list-inline item-details">
                                <li>Tools: 
                                    <strong>Visual Studio, TFS, C#
                                    </strong>
                                </li>
                            </ul>                          
                            <ul className="list-inline item-details">
              
                                <li>Date: 
                                    <strong>August 2016
                                    </strong>
                                </li>

                            </ul>
                            <button type="button" className="btn btn-default" data-dismiss="modal"><i className="fa fa-times"></i> Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div className="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog" aria-hidden="true">
        <div className="modal-content">
            <div className="close-modal" data-dismiss="modal">
                <div className="lr">
                    <div className="rl">
                    </div>
                </div>
            </div>
            <div className="container">
                <div className="row">
                    <div className="col-lg-8 col-lg-offset-2">
                        <div className="modal-body">
                            <h2>Web APP Mobile Automation Tool</h2>
                            <hr className="star-primary"/>
                            <video className="img-responsive img-centered" controls>
                                  <source src="/src/doc/demo.mp4" type="video/mp4"/>

                            </video>
                            <br/>
                            <p>This tool can reuse existing selenium web browser test cases to run on Android and IOS simulators.
It saved hundreds of hours in writing new test cases and increased stability of web application on mobile devices.
</p>
                                                                                <ul className="list-inline item-details">
                                <li>Tools: 
                                    <strong>C#, Android SDK, Appium, Seleium
                                    </strong>
                                </li>
                            </ul>  
                            <ul className="list-inline item-details">
                                <li>Date: 
                                    <strong>August 2017
                                    </strong>
                                </li>
  
                            </ul>
                            <button type="button" className="btn btn-default" data-dismiss="modal"><i className="fa fa-times"></i> Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div className="portfolio-modal modal fade" id="portfolioModal4" tabindex="-1" role="dialog" aria-hidden="true">
        <div className="modal-content">
            <div className="close-modal" data-dismiss="modal">
                <div className="lr">
                    <div className="rl">
                    </div>
                </div>
            </div>
            <div className="container">
                <div className="row">
                    <div className="col-lg-8 col-lg-offset-2">
                        <div className="modal-body">
                            <h2>Project Title</h2>
                            <hr className="star-primary"/>
                            <img src={game} className="img-responsive img-centered" alt=""/>
                            <p>Use this area of the page to describe your project. The icon above is part of a free icon set by <a href="https://sellfy.com/p/8Q9P/jV3VZ/">Flat Icons</a>. On their website, you can download their free set with 16 icons, or you can purchase the entire set with 146 icons for only $12!</p>
                            <ul className="list-inline item-details">
                                <li>Client:
                                    <strong><a href="http://startbootstrap.com">Start Bootstrap</a>
                                    </strong>
                                </li>
                                <li>Date:
                                    <strong><a href="http://startbootstrap.com">April 2014</a>
                                    </strong>
                                </li>
                                <li>Service:
                                    <strong><a href="http://startbootstrap.com">Web Development</a>
                                    </strong>
                                </li>
                            </ul>
                            <button type="button" className="btn btn-default" data-dismiss="modal"><i className="fa fa-times"></i> Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div className="portfolio-modal modal fade" id="portfolioModal5" tabindex="-1" role="dialog" aria-hidden="true">
        <div className="modal-content">
            <div className="close-modal" data-dismiss="modal">
                <div className="lr">
                    <div className="rl">
                    </div>
                </div>
            </div>
            <div className="container">
                <div className="row">
                    <div className="col-lg-8 col-lg-offset-2">
                        <div className="modal-body">
                            <h2>Project Title</h2>
                            <hr className="star-primary"/>
                            <img src={safe} className="img-responsive img-centered" alt=""/>
                            <p>Use this area of the page to describe your project. The icon above is part of a free icon set by <a href="https://sellfy.com/p/8Q9P/jV3VZ/">Flat Icons</a>. On their website, you can download their free set with 16 icons, or you can purchase the entire set with 146 icons for only $12!</p>
                            <ul className="list-inline item-details">
                                <li>Client:
                                    <strong><a href="http://startbootstrap.com">Start Bootstrap</a>
                                    </strong>
                                </li>
                                <li>Date:
                                    <strong><a href="http://startbootstrap.com">April 2014</a>
                                    </strong>
                                </li>
                                <li>Service:
                                    <strong><a href="http://startbootstrap.com">Web Development</a>
                                    </strong>
                                </li>
                            </ul>
                            <button type="button" className="btn btn-default" data-dismiss="modal"><i className="fa fa-times"></i> Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div className="portfolio-modal modal fade" id="portfolioModal6" tabindex="-1" role="dialog" aria-hidden="true">
        <div className="modal-content">
            <div className="close-modal" data-dismiss="modal">
                <div className="lr">
                    <div className="rl">
                    </div>
                </div>
            </div>
            <div className="container">
                <div className="row">
                    <div className="col-lg-8 col-lg-offset-2">
                        <div className="modal-body">
                            <h2>Project Title</h2>
                            <hr className="star-primary"/>
                            <img src={submarine} className="img-responsive img-centered" alt=""/>
                            <p>Use this area of the page to describe your project. The icon above is part of a free icon set by <a href="https://sellfy.com/p/8Q9P/jV3VZ/">Flat Icons</a>. On their website, you can download their free set with 16 icons, or you can purchase the entire set with 146 icons for only $12!</p>
                            <ul className="list-inline item-details">
                                <li>Client:
                                    <strong><a href="http://startbootstrap.com">Start Bootstrap</a>
                                    </strong>
                                </li>
                                <li>Date:
                                    <strong><a href="http://startbootstrap.com">April 2014</a>
                                    </strong>
                                </li>
                                <li>Service:
                                    <strong><a href="http://startbootstrap.com">Web Development</a>
                                    </strong>
                                </li>
                            </ul>
                            <button id="btnSubmit" type="button" className="btn btn-default" data-dismiss="modal"><i className="fa fa-times"></i> Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        </div>
    );
  }
}

export default ProjectDetails;
