import React, { Component } from 'react';

class Project extends Component{
    constructor(props){
        super(props);
        this.state = {
            hrefLink : this.props.hrefLink,
            picture : this.props.picture
        }
    }
    
    render() {
        return (
            <div className="col-sm-4 portfolio-item">
                <a href={this.state.hrefLink} className="portfolio-link" data-toggle="modal">
                    <div className="caption">
                        <div className="caption-content">
                            <i className="fa fa-search-plus fa-3x"></i>
                        </div>
                    </div>
                    <img src={this.state.picture} className="img-responsive" alt="Cabin" />
                </a>
            </div>
        );
    }
}

export default Project;
