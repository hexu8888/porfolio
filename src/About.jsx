import React, { Component } from 'react';

class About extends Component {
  render() {
    return (
        <section className="success" id="about">
        <div className="container">
            <div className="row">
                <div className="col-lg-12 text-center">
                    <h2>About</h2>
                        <hr className="star-primary"></hr>
                </div>
            </div>
            <div className="row">
                <div className="col-lg-8 col-lg-offset-2 text-center">
                    <p>Howdy, I am <b>Ken He</b>. I graduated from UCSD with a bachelor degree in Computer Science. I enjoy my everyday life. I like playing basketball and ping pong, hiking, and workout. I am always energetic at work. I enthuse about building software to solve people’s problems and improve their efficiency. </p>

                </div>
            </div>
                                    <hr></hr>

            <br/><br/>
                                                <div className="row">

                <div className="col-sm-4 text-center"><p>
<i className="fa fa-graduation-cap fa-5x"></i><br/>

</p>                    <div><p>UCSD - <b>Computer Science</b></p><p>Graduated in <b>2017</b></p><p>GPA: <b>3.4</b></p></div><br/>
                </div>
                                <div className="col-sm-4 text-center"><p>
<i className="fa fa-briefcase fa-5x"></i>
</p>
                                    <div><p>Mitchell Int. - <b>Apr 2017 to Present</b></p><p>UCSD CSE Tutor - <b>Spring 2016</b></p><p>Idea Health & Fitness - <b>Spring 2016</b></p><p>Megamadz - <b>Summer 2015</b></p></div><br/>
                </div>
                <div className="col-sm-4 text-center"><p>
<i className="fa fa-star fa-5x"></i>
</p>
                                                        <div><p>Mitchel Inc. Technical Forum <b>Host</b></p><p>UCSD WIC Hackathon <b>Mentor</b></p><p>UCSD CSA <b>Technical Leader</b></p></div><br/>
                </div>
                                                    <div id="resume"></div>
            </div>
                                                                                                    <div className="row">

                <div className="col-lg-8 col-lg-offset-2 text-center">
                    <a href="/src/doc/resume_kenhe.docx" className="btn btn-lg btn-outline">
                        <i className="fa fa-download"></i> Download Resume(.docx)
                    </a>
                </div>
                                                                    <div className="col-lg-8 col-lg-offset-2 text-center">
                    <a href="/src/doc/resume_kenhe.pdf" className="btn btn-lg btn-outline" download="resume_kenhe">
                        <i className="fa fa-download"></i> Download Resume(.pdf)
                    </a>
                </div>
            </div>
        </div>
    </section>
    );
  }
}

export default About;
