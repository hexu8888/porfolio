import React, { Component } from 'react';
import 'bootstrap/dist/js/bootstrap';
import './vendor/bootstrap/css/bootstrap.min.css';
import './vendor/font-awesome/css/font-awesome.min.css';
import './css/freelancer.css';
import Navigate from './Navigate';
import Header from './Header';
import Porfolio from './Porfolio';
import About from './About';
import Contact from './Contact';
import Footer from './Footer';
import ProjectDetails from './ProjectDetails';

class App extends Component {
  render() {
    return (
        <div>
            <Navigate />
            <Header />
            <About />
            <Porfolio />
            <Footer />
            <ProjectDetails />
        </div>
    );
  }
}

export default App;
