
import React, { Component } from 'react';
import Project from './Project';

var cabin = require('./img/portfolio/bfh_512.png');
var cake = require('./img/portfolio/crt.png');
var circus = require('./img/portfolio/mat.png');
var game = require('./img/portfolio/game.png');
var safe = require('./img/portfolio/safe.png');
var submarine = require('./img/portfolio/submarine.png');


class Porfolio extends Component {
    constructor(props){
        super(props);
        this.state = {
            projects : [
                {hrefLink: "#portfolioModal1", picture : cabin},
                {hrefLink: "#portfolioModal2", picture : cake},
                {hrefLink: "#portfolioModal3", picture : circus},

            ]
        };
    }
  render() {
    return (
        <div>
        <section id="portfolio">
            <div className="container">
                <div className="row">
                    <div className="col-lg-12 text-center">
                        <h2>Portfolio</h2>
                        <hr className="star-primary"></hr>
                    </div>
                </div>
                <div className="row">
                    {this.state.projects.map((x, i) =>
                        <Project hrefLink = {x.hrefLink} picture = {x.picture} />
                    )}
                </div>
                
            </div>
        </section>
        </div>
    );
  }
}

export default Porfolio;
