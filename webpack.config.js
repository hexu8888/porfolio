var webpack = require('webpack');
var path = require('path');

var BUILD_DIR = path.resolve(__dirname, 'build');
var APP_DIR = path.resolve(__dirname, 'src/');

var config = {
  entry: APP_DIR + '/index.jsx',
  output: {
    path: BUILD_DIR,
    filename: 'bundle.js',
    publicPath: 'src/'
  },
  module : {
    loaders : [
      {
        test : /\.jsx?$/,
        include : APP_DIR,
        loader : 'babel-loader'
      },
      { 
        test: /\.css$/,
        include : APP_DIR,
        loader: 'style-loader!css-loader'
      },
        {
          test: /\.(png|jpg|ttf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
          loader: 'file-loader',
          options: {
            name: 'public/'+'[path][name].[hash].[ext]',
          },
        },
    ]
  },
  plugins: [
    new webpack.ProvidePlugin({
        '$': 'jquery',
        'jQuery': 'jquery',
        'window.jQuery': 'jquery'
    })
  ],
  resolve : {
    extensions: ['.js', '.jsx']
  }

};

module.exports = config;
